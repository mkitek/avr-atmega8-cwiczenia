// Program demonstrujący działanie wyświetlacza 7 segmentów LED
// Port B wykorzystany do podłączenia wyświetlacza

#include <avr/io.h>
// #define F_CPU 1000000 // zdefiniowane juz w Makefile§§
#include <util/delay.h>

int main(void)
{
	int8_t i;
	// Tablica przechowująca wartości reprezentujące konkretne liczby na LCD
	unsigned char liczba_LED[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};

	// Ustawienie wszystkich linii portu B i C jako wyjściowych
	DDRB = 0xFF;
	DDRC = 0xFF;

	// Ustaw stan wysoki na zerowej linii portu C
	PORTC = 0x01;

	for(;;) {
		for(i=0;i<9;i++)
		{
			// Wyślij cyfrę na wyświetlacz LED
			PORTB = liczba_LED[i];
			_delay_ms(250);
		}
		for(i=9;i>=0;i--)
		 {
			PORTB = liczba_LED[i];

			// Zapal kropkę
			PORTB &= ~(_BV(7));
			_delay_ms(250);
		}
	}

	return 0;
}
