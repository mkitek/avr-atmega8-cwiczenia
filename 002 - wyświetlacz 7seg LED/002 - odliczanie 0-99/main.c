// Program demonstrujący działanie wyświetlacza 2 x 7seg LED
// Wyświetla wartości od 0 do 99

#include <avr/io.h>
// #define F_CPU 1000000 // zdefiniowane juz w Makefile§§
#include <util/delay.h>

int main(void)
{
	int8_t i, k, m;
	// Tablica przechowująca wartości reprezentujące konkretne liczby na LCD
	unsigned char liczba_LED[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};

	// Ustawienie wszystkich linii portu B i C jako wyjściowych
	DDRB = 0xFF;
	DDRC = 0xFF;

	for(;;) {
		for(i=0;i<=9;i++)
		{
			for(k=0;k<=9;k++)
			{
				for(m=1;m<=10;m++)
				{
					// Pierwszy segment
					PORTC = 0x01;
					PORTB = liczba_LED[k];
					_delay_ms(5);

					// Drugi semgment
					PORTC = 0x02;
					PORTB = liczba_LED[i];			
					_delay_ms(5);
				}
			}
		}
	}

	return 0;
}
