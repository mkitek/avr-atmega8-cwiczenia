// Zapelenie diody LED podłączonej do linii PB4 i PC1
// schemat: 3.31

#include <avr/io.h>

int main(void)
{
	// Ustawienie linii PB4 jako wyjściowej
	DDRB = 0b00010000;
	// Ustawienie linii PC1 jako wyjściowej
	DDRC = 0b00000010;

	// Ustawienie stanu wysokiego na linii 4 portu B
	PORTB = 0b00010000;

	for(;;);

	return 0;
}