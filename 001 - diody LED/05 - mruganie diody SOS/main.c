// Ćwiczenie 5. Mruganie sygnału SOS diody LED podłączonej do portu PB0 i PB1 
// schemat: 4.1

#include <avr/io.h>
// #define F_CPU 1000000 // zdefiniowane juz w Makefile§§
#include <util/delay.h>

int main(void)
{
	int8_t i;

	// Ustawienie linii PB0 i PB1 jako wyjściowej
	// linie:76543210
	DDRB = 0b00000011;

	for(;;) {
		// S
		for(i=0;i<2;i++)
		{
			// Ustawienie stanu wysokiego na linii 0 portu B
			// linie: 76543210
			PORTB = 0b00000001;
			// Poczekaj 'wait' ms
			_delay_ms(250);
			// Ustawienie stanu niskiego na linii PB0
			PORTB = 0;
			// Poczekaj 'wait' ms
			_delay_ms(250);
		}
		// O
		for(i=0;i<2;i++)
		{
			PORTB = 0b00000001;
			_delay_ms(250);
			_delay_ms(250);
			PORTB = 0;
			_delay_ms(250);
			_delay_ms(250);
		}
		// S
		for(i=0;i<2;i++)
		{
			PORTB = 0b00000001;
			_delay_ms(250);
			PORTB = 0;
			_delay_ms(250);
		}
	}

	return 0;
}
