// Ćwiczenie 3. Zaświecenie trzech diod LED podłączonych w konfiguracji:
// 1. VCC PC2
// 2. PC0 PC1
// 3. PB3 GND
// schemat: 3.32

#include <avr/io.h>

int main(void)
{
	// Ustawienie linii PB3 jako wyjściowej
	// linie:76543210
	DDRB = 0b00001000;

	// Ustawienie linii PC0, PC1, PC2 jako wyjściowej
	// linie:76543210
	DDRC = 0b00000111;

	// Ustawienie stanu wysokiego na linii 3 portu B
	// linie: 76543210
	PORTB = 0b00001000;

	// Ustawienie stanu wysokiego na linii PC0
	PORTC = 0b00000001;

	for(;;);

	return 0;
}