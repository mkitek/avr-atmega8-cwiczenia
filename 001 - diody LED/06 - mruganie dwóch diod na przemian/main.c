// Ćwiczenie 6. Mruganie dwóch diod LED na przemian
// schemat: 4.8

#include <avr/io.h>
// #define F_CPU 1000000 // zdefiniowane juz w Makefile§§
#include <util/delay.h>

int main(void)
{

	// Ustawienie linii PB0, PB1, PB2, PB3 jako wyjściowej
	// linie:76543210
	DDRB = 0b00001111;

	for(;;) {
		// Ustawienie stanu wysokiego na linii 0 portu B
		// linie: 76543210
		PORTB = 0b00000001;
		// Poczekaj 'wait' ms
		_delay_ms(250);
		// Ustawienie stanu niskiego na linii PB0
		PORTB = 0;
		// Poczekaj 'wait' ms
		_delay_ms(250);

		// Ustawienie stanu wysokiego na linii 2 portu B
		// linie: 76543210
		PORTB = 0b00000100;
		// Poczekaj 'wait' ms
		_delay_ms(250);
		// Ustawienie stanu niskiego na linii PB0
		PORTB = 0;
		// Poczekaj 'wait' ms
		_delay_ms(250);
	}

	return 0;
}
