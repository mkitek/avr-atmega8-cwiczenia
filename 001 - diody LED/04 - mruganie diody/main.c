// Ćwiczenie 4. Mruganie diody LED podłączonej do portu PB0 i PB1
// schemat: 4.1

#include <avr/io.h>
// #define F_CPU 1000000 // zdefiniowane juz w Makefile§§
#include <util/delay.h>

int main(void)
{
	// Ustawienie linii PB0 i PB1 jako wyjściowej
	// linie:76543210
	DDRB = 0b00000011;

	for(;;) {
		// Ustawienie stanu wysokiego na linii 0 portu B
		// linie: 76543210
		PORTB = 0b00000001;

		// Poczekaj 250 ms
		_delay_ms(250);

		// Ustawienie stanu niskiego na linii PB0
		PORTB = 0;

		// Poczekaj 250 ms
		_delay_ms(250);	
	}

	return 0;
}
		