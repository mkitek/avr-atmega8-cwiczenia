// Zapelenie diody LED podłączonej do linii PB0 i PB1
// schemat: 3.3

#include <avr/io.h>

int main(void)
{
	// Ustawienie linii 0 i 1 portu B jako wyjściowych
	// 0x03 = 0b00000011 (PB0, PB1)
	DDRB = 0x03;

	// Ustawienie stanu wysokiego na linii 0 portu B
	PORTB = 0x01;

	for(;;);

	return 0;
}